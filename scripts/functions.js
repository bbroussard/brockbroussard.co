let emailLink = document.querySelector('[href="mailto:bbroussard@protonmail.com"]');
let resumeLink = document.querySelector('#resume-button');
let helloBlock = document.getElementById("hello");
let currentYear = document.getElementById("year");
const captchaButton = document.getElementById('captcha-button');

let x = Math.floor(Math.random() * Math.floor(50));
let y = Math.floor(Math.random() * Math.floor(50));

currentYear.innerHTML = new Date().getFullYear();

function cycleGreetings() {
	let greetingList = [
		"Hello!",
		`\u00A1Hola!`,
		`\u3053\u3093\u306B\u3061\u306F\uFF01`, //"こんにちは"
		"Bonjour!",
		"Hallo!",
		"Ciao!"
	]
	helloBlock.innerHTML = greetingList[Math.floor(Math.random() * Math.floor(greetingList.length))];
}

function hideButton(button) {
	button.classList.add('hidden');
	button.classList.remove('big-button');
}

function showButton(button) {
	button.classList.remove('hidden');
	button.classList.add('big-button');
}

//function disableLink(targetLink) {
function hideLinks() {
	emailLink.href = '';
	hideButton(emailLink);
}

//function enableLink(targetLink) {
function revealLinks() {	
	emailLink.href = "mailto:bbroussard@protonmail.com";
	showButton(emailLink);
}

function fadeIn() {
	helloBlock.classList.add('fade-in');
	helloBlock.classList.remove('fade-out');
}

function fadeOut() {
	helloBlock.classList.add('fade-out');
	helloBlock.classList.remove('fade-in');
}

function checkAnswer(event) {
	const input = document.getElementById('captcha-field');
	if (input.value !== (x + y).toString()) {
		alert("You did not enter the correct answer. Please try again.");
		event.preventDefault();
	}
}

if (document.title === "Brock Broussard's Personal Site") {
	cycleGreetings();
	window.setInterval(cycleGreetings, 5000)
	window.setInterval(fadeIn, 5000)
	window.setInterval(fadeOut, 4999)
}

if (document.title === "Demos | Brock Broussard's Personal Site") {}

if (document.title === "About | Brock Broussard's Personal Site") {
	document.getElementById("x").innerHTML = x;
	document.getElementById("y").innerHTML = y;
	hideLinks();
	captchaButton.addEventListener("click", function(){checkAnswer()});
}
